/*
 * @author Ryan Sutana
 * @description Pass all datas requested through wp-ajax
 * @since v 1.0
 */

jQuery(document).ready(function($) {
	
	/**
	 * Lost password
	 * @param  {[type]} ){		                     var submit [description]
	 * @return {[type]}       [description]
	 */
    $("#frm_lostpassword").submit(function(){
		
        var submit		= $("#frm_lostpassword #submit"),
            message    	= $(".nf-form-wrap #message"),
            contents 	= {
                action:		'lostpassword',
                nonce:		this.trabajo_lostpassword_nonce.value,
                user_login:	this.user_login.value
            };
        
        // disable button onsubmit to avoid double submision
        submit.attr("disabled", "disabled").addClass('disabled');
        
		$.post( theme_ajax.url, contents, function( data ){
            submit.removeAttr("disabled").removeClass('disabled');
            
			// display return data
			message.html( data );
            
        });
        
        return false;
    });

	
});