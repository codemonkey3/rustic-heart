<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<?php
						if( isset( $options['footer_text'] ) ) {
							echo $options['footer_text'];
						}
					?>
				</div>
			</div>
		</div>
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet"> 
<?php
	// include WP footer for hook and styles
	wp_footer();

	// display tracking or analytics code
	if( isset( $options['analytics'] ) ) {
		echo $options['analytics'];
	}
?>

</body>
</html>
