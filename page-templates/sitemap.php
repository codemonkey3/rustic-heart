<?php
/**
 * Template Name: Sitemap
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

get_header(); ?>
	
	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row justify-content-center">
				<div class="col-md-10">

					<div id="primary" class="content-area">

						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'page' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>

					</div><!-- #primary -->

				</div>
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->

<?php
get_footer();
