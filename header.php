<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}
?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'rs-theme' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="site-header" role="banner" style="<?php echo $masthead_style; ?>">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-4">

					<div class="site-branding">
						<?php
						if ( is_front_page() && is_home() ) { ?>
							<h1 class="site-title mb-0">

								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php if( isset( $options['logo'] ) ) { ?>

										<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

									<?php } else { ?>

										<?php bloginfo( 'name' ); ?>
										
									<?php } ?>
								</a>

							</h1>
						<?php } else { ?>
							<p class="site-title mb-0">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php if( isset( $options['logo'] ) ) { ?>

										<img src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

									<?php } else { ?>

										<?php bloginfo( 'name' ); ?>

									<?php } ?>
								</a>
							</p>
						<?php
						}

						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
						<?php
						endif; ?>
					</div><!-- .site-branding -->
					
				</div>
				
				<div class="col-lg-8">
					<div class="phone-holder">
						<?php 
							if( isset( $options['phone_number'] ) ) { ?>
								<p>
									Call Now
									<a href="tel:<?php echo $options['phone_number']; ?>">
										<span class="phone"><?php echo $options['phone_number']; ?></span>
									</a>
								</p>
							<?php }
						?>
					</div>
				</div>

			</div>
		</div>
	</header><!-- #masthead -->

	<nav id="site-navigation" class="main-navigation" role="navigation">
		
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-12">

					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
					
					<?php wp_nav_menu( array( 
						'theme_location' => 'primary',
						'menu_id' 		 => 'primary-menu',
						'menu_class' 	 => 'primary-menu',
						'container' 	 => 'ul'
					) ); ?>
				
				</div>
			</div>
		</div>

	</nav><!-- #site-navigation -->

	<div id="content" class="site-content">
